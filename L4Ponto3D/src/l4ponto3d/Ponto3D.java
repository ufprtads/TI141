/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l4ponto3d;

/**
 *
 * @author adams
 */
public class Ponto3D {
    private double x, y, z, intensidade;
    private int cor; 
    
    public Ponto3D() {
        this(0,0,0,0,1.0);
    }
    
    public Ponto3D(double x, double y, double z, int cor, double intensidade) {
        this.x = x; 
        this.y = y; 
        this.z = z; 
        this.cor = cor; 
        this.intensidade = intensidade; 
    }

    public double getX() {
        return this.x;
    }
    
    public void setX(double x) {
        this.x = x; 
    }
    
    public double getY() {
        return this.y; 
    }
    
    public void setY(double y) {
        this.y = y;
    }
    
    public double getZ() {
        return this.z;
    }
    
    public void setZ(double z) {
        this.z = z; 
    }
    
    public int getCor() {
        return this.cor; 
    }
    
    public void setCor(int cor) {
        this.cor = cor;
    }
    
    public double getIntensidade() {
        return this.intensidade;
    }
    
    public void setIntensidade(double intensidade) {
        this.intensidade = intensidade; 
    }
    
    
    public double calculaDistancia(Ponto3D p) {
        return Math.sqrt(Math.pow(p.getX() - this.x, 2) + Math.pow(p.getY() - this.y, 2) + Math.pow(p.getZ() - this.z, 2));
    }
}
