/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s07excecoes.rh;

/**
 *
 * @author adams
 */
public class Analista extends Funcionario {
    public Analista(String nome, int idade, double salario, String dataAdmissao) {
        super(nome,idade,salario,dataAdmissao);
    }
    
    public double getBonus() {
        return getSalario();
    }
}
