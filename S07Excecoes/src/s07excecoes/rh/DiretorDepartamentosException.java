/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s07excecoes.rh;

/**
 *
 * @author adams
 */
class DiretorDepartamentosException extends Exception {

    public DiretorDepartamentosException(int length) {
        super("Número de departamentos não pode ser menor que 2 (informado: "+length+")");
    }
    
}
