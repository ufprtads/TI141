/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s07excecoes.rh;

/**
 *
 * @author adams
 */
class GerenteNumFuncionariosException extends Exception {
    public GerenteNumFuncionariosException(int numFuncionarios) {
        super("O número de funcionários deve ser no mínimo 5 (informados: "+numFuncionarios+")");
    }
}
