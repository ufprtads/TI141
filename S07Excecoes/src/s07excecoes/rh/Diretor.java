/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s07excecoes.rh;

/**
 *
 * @author adams
 */
public class Diretor extends Funcionario {
    
    public String[] departamentos;
    
    public Diretor(String nome, int idade, double salario, String dataAdmissao, int departamentos) throws DiretorDepartamentosException {
        super(nome,idade,salario,dataAdmissao);
        setDepartamentos(new String[departamentos]);
    }

    public String[] getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(String[] departamentos) throws DiretorDepartamentosException {
        if(departamentos.length < 2)
            throw new DiretorDepartamentosException(departamentos.length);
        this.departamentos = departamentos;
    }

    @Override
    public double getBonus() {
        return 4*getSalario() + 3000*departamentos.length;
    }

}
