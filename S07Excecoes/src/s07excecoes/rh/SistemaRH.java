/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s07excecoes.rh;

/**
 *
 * @author adams
 */
public class SistemaRH {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            Funcionario[] listaFuncionarios = new Funcionario[13];
            listaFuncionarios[0] = new Diretor("Diretor Geral", 55, 12000, "10/10/2005", 2);
            Diretor dir = (Diretor) listaFuncionarios[0];
            dir.setDepartamentos(new String[]{"Financeiro", "Operacional"});
            listaFuncionarios[1] = new Gerente("Gerente Financeiro", 36, 6000, "10/05/2012", 5);
            Gerente financeiro = (Gerente) listaFuncionarios[1];
            listaFuncionarios[2] = new Gerente("Gerente Operacional", 41, 5200, "10/06/2013", 5);
            Gerente operacional = (Gerente) listaFuncionarios[2];
            Funcionario[] oper = new Funcionario[5];
            int mi = 3;
            for(int i = 0; i < 5; ++i) {
                oper[i] = new Analista("Analista "+i, 35, 1900, "22/02/2005");
                listaFuncionarios[mi++] = oper[i];
            }
            Funcionario[] finan = new Funcionario[5];
            for(int i = 0; i < 5; ++i) {
                finan[i] = new Analista("Analista "+i, 35, 1900, "22/02/2005");
                listaFuncionarios[mi++] = finan[i];
            }
            financeiro.setFuncionarios(finan);
            operacional.setFuncionarios(oper);
            imprimeRelatorio(listaFuncionarios);
        } catch (DiretorDepartamentosException diretorDepartamentosException) {
            System.err.println(diretorDepartamentosException.getMessage());
        } catch (GerenteNumFuncionariosException gerenteNumFuncionariosException) {
            System.err.println(gerenteNumFuncionariosException.getMessage());
        }
    }
    
    
    public static void imprimeRelatorio(Funcionario[] listaFuncionarios) {
        System.out.println("Relatório de Funcionários (cargo/nome/dt. admissao)");
        for(Funcionario func : listaFuncionarios) {
            System.out.println(func.getClass().getName()+": "+func.getNome()+"("+func.getDataAdmissao()+")");
        }
    }
}
