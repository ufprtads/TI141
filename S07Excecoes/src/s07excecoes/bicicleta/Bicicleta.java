/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s07excecoes.bicicleta;

/**
 *
 * @author adams
 */
class Bicicleta {

    int cadencia = 0;
    int velocidade = 0;
    int marcha = 1;

    public Bicicleta() throws BicicletaRPMBelowZeroException, BicicletaGearTooLowException, BicicletaSpeedTooHighException, BicicletaGearTooHighException, BicicletaSpeedTooLowException {
        this(0,0,0);
    }

    public Bicicleta(int cadencia, int velocidade, int marcha) throws BicicletaRPMBelowZeroException, BicicletaGearTooLowException, BicicletaSpeedTooHighException, BicicletaGearTooHighException, BicicletaSpeedTooLowException {
        mudarCadencia(cadencia);
        aumentarVelocidade(velocidade);
        mudarMarcha(marcha);
    }

    void mudarCadencia(int novoValor) throws BicicletaRPMBelowZeroException {
        if (novoValor < 0) {
            throw new BicicletaRPMBelowZeroException();
        }
        cadencia = novoValor;
    }

    void mudarMarcha(int novoValor) throws BicicletaGearTooLowException, BicicletaGearTooHighException {
        if (novoValor < 0) {
            throw new BicicletaGearTooLowException();
        }
        if (novoValor > 24) {
            throw new BicicletaGearTooHighException();
        }
        marcha = novoValor;
    }

    void aumentarVelocidade(int incremento) throws BicicletaSpeedTooHighException, BicicletaSpeedTooLowException {
        if ((velocidade + incremento) > 100) {
            throw new BicicletaSpeedTooHighException();
        }
        if ((velocidade + incremento) < 0) {
            throw new BicicletaSpeedTooLowException();
        }
        velocidade += incremento;
    }

    void aplicarFreios(int decremento) throws BicicletaSpeedTooLowException, BicicletaSpeedTooHighException {
        if ((velocidade - decremento) > 100) {
            throw new BicicletaSpeedTooHighException();
        }
        if ((velocidade - decremento) < 0) {
            throw new BicicletaSpeedTooLowException();
        }
        velocidade = velocidade - decremento;
    }

    void printStates() {
        System.out.println("cadencia=" + cadencia + " velocidade=" + velocidade + "marcha=" + marcha);
    }
}
