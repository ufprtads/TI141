/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s07excecoes.bicicleta;

/**
 *
 * @author adams
 */
class BicicletaSpeedTooLowException extends Exception {

    public BicicletaSpeedTooLowException() {
        super("Velocidade inferior à 0 km/h");
    }
    
}
