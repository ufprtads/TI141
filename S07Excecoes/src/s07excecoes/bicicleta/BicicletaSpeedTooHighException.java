/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s07excecoes.bicicleta;

/**
 *
 * @author adams
 */
public class BicicletaSpeedTooHighException extends Exception {
    public BicicletaSpeedTooHighException() {
        super("Velocidade acima de 100km/h");
    }
}
