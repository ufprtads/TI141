/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s07excecoes.bicicleta;

/**
 *
 * @author adams
 */
public class UsaBicicleta {
    public static void main(String args[]) {
        //velocidade máxima
        try {
            Bicicleta caloi = new Bicicleta();
            caloi.aumentarVelocidade(200);
        } catch (BicicletaRPMBelowZeroException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaGearTooLowException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaSpeedTooHighException ex) {
            System.err.println(ex.getMessage());
        } catch (BicicletaGearTooHighException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaSpeedTooLowException ex) {
            System.err.println("Não deve acontecer.");
        }
        //velocidade mínima
        try {
            Bicicleta monark = new Bicicleta();
            monark.aplicarFreios(50);
        } catch (BicicletaRPMBelowZeroException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaGearTooLowException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaSpeedTooHighException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaGearTooHighException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaSpeedTooLowException ex) {
            System.err.println(ex.getMessage());
        }
        //rpm abaixo de zero
        try {
            Bicicleta sundown = new Bicicleta();
            sundown.mudarCadencia(-1);
        } catch (BicicletaRPMBelowZeroException ex) {
            System.err.println(ex.getMessage());
        } catch (BicicletaGearTooLowException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaSpeedTooHighException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaGearTooHighException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaSpeedTooLowException ex) {
            System.err.println("Não deve acontecer.");
        }
        //marcha menor que zero 
        try { 
            Bicicleta mongar = new Bicicleta();
            mongar.mudarMarcha(-1);
        } catch (BicicletaRPMBelowZeroException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaGearTooLowException ex) {
            System.err.println(ex.getMessage());
        } catch (BicicletaSpeedTooHighException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaGearTooHighException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaSpeedTooLowException ex) {
            System.err.println("Não deve acontecer.");
        }
        try {
            //marcha maior que 24
            Bicicleta mountainbike = new Bicicleta();
            mountainbike.mudarMarcha(25);
        } catch (BicicletaRPMBelowZeroException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaGearTooLowException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaSpeedTooHighException ex) {
            System.err.println("Não deve acontecer.");
        } catch (BicicletaGearTooHighException ex) {
            System.err.println(ex.getMessage());
        } catch (BicicletaSpeedTooLowException ex) {
            System.err.println("Não deve acontecer.");
        }
        try {
            //testando erro no construtor
            Bicicleta desconhecida = new Bicicleta(-1,-1,-1);
        } catch (BicicletaRPMBelowZeroException ex) {
            System.err.println(ex.getMessage());
        } catch (BicicletaGearTooLowException ex) {
            System.err.println(ex.getMessage());
        } catch (BicicletaSpeedTooHighException ex) {
            System.err.println(ex.getMessage());
        } catch (BicicletaGearTooHighException ex) {
            System.err.println(ex.getMessage());
        } catch (BicicletaSpeedTooLowException ex) {
            System.err.println(ex.getMessage());
        }
        try {
            //construçào com sucesso, mas alterações com exceções
            Bicicleta nova = new Bicicleta(250, 25, 10);
            System.out.println("Construída com sucesso!");
            nova.aplicarFreios(50);
        } catch (BicicletaRPMBelowZeroException ex) {
            System.err.println(ex.getMessage());
        } catch (BicicletaGearTooLowException ex) {
            System.err.println(ex.getMessage());
        } catch (BicicletaSpeedTooHighException ex) {
            System.err.println(ex.getMessage());
        } catch (BicicletaGearTooHighException ex) {
            System.err.println(ex.getMessage());
        } catch (BicicletaSpeedTooLowException ex) {
            System.err.println(ex.getMessage());
        }
    }
}
