/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s07excecoes.media;

import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author adams
 */
public class Media {
    private int numElementos = 0; 
    private double somaElementos = 0;
    private boolean runFlag = true; 
    public static void main(String args[]) {
        Media md = new Media();
        md.run();
    }
    
    private void run() {
        runFlag = true;
        msgInit();
        do {
            try {
                getInput();
            } catch(Exception ex) {
                System.err.println("Erro de entrada.");
                System.err.println(ex.getMessage());
            }
        } while(runFlag);
        if(numElementos > 0) {
            printData();
        }
    }
    
    private void getInput() throws Exception {
        System.out.print("Número "+(numElementos+1)+"=");
        Scanner scn = new Scanner(System.in);
        String cap = scn.next();
        if(Pattern.matches("^[0-9.]+$", cap)) {
            somaElementos += Double.parseDouble(cap);
            ++numElementos;
        } else if(Pattern.matches("^[s|S]$", cap)) {
            runFlag = false; 
        } else {
            throw new Exception(cap+" não é um valor numérico válido. Tente novamente ou digite S para sair.");
        }
    }
    
    private void printData() {
        System.out.println("A soma dos números digitados é="+somaElementos);
        System.out.println("A média dos números digitados é="+(somaElementos/numElementos));
    }
    private void msgInit() {
        System.out.println("Informe os números na sequencia solicitada.");
        System.out.println("Para sair digite 'S'");
    }
}
