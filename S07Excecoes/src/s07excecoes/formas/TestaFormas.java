package s07excecoes.formas;

public class TestaFormas {

    public static void main(String args[]) {
        try {
            Circunferencia c = new Circunferencia(1.5);
            c.setMedida(-1, 5);
            System.out.println("areaCirc = " + c.area());

            Retangulo r = new Retangulo(-1, 4.0);
            System.out.println("areaRet = " + r.area());

            Forma formas[] = new Forma[3];
            formas[0] = c;
            formas[1] = r;
            formas[2] = new Triangulo(1.0, 2.0, 2.0);
            Triangulo t = (Triangulo) formas[2];
            if (formas[2] instanceof Circunferencia) {
                Circunferencia c1 = (Circunferencia) formas[2];
            }
            for (int i = 0; i < formas.length; i++) {
                System.out.println(formas[i]);
                System.out.println("area = " + formas[i].area());
            }
            desenhaTodosAsFormasNaTela(formas);
            ArquivoGIF arq = new ArquivoGIF();
            Desenhavel[] arrayDesenhavel = new Desenhavel[3];
            arrayDesenhavel[0] = c;
            arrayDesenhavel[1] = r;
            arrayDesenhavel[2] = arq;
            desenhaTodosAsFormasNaTela(arrayDesenhavel);
        } catch (IllegalArgumentException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public static void desenhaTodosAsFormasNaTela(Desenhavel[] formas) {
        for (Desenhavel d : formas) {
            d.desenhaFormaNaTela();
        }

    }
}
