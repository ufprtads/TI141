/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l4circunferencia;

/**
 *
 * @author adams
 */
public class Circunferencia {
    private double raio; 
    
    public Circunferencia() {
        this(0.0);
    }
    public Circunferencia(double raio) {
        this.raio = raio; 
    }
    
    public double getRaio() {
        return this.raio;
    }
    
    public void setRaio(double raio) {
        this.raio = raio; 
    }
    
    public double Area() {
        return this.raio * 2 * Math.PI;
    }
}
