/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import l4circunferencia.Circunferencia;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author adams
 */
public class UsaCircunferencia {
        @Test
        public void TesteArea1() {
            Circunferencia circ = new Circunferencia(5.0);
            assertEquals(5.0, circ.getRaio(), 0.0);
            assertEquals(2*5*Math.PI, circ.Area(), 0.0);
        }
        
        @Test 
        public void TesteArea2() {
            Circunferencia circ = new Circunferencia(10.0);
            assertEquals(2*10*Math.PI, circ.Area(), 0.0);
            
        }
        
        @Test
        public void TesteRaio1() {
            Circunferencia circ = new Circunferencia(10);
            circ.setRaio(25);
            assertEquals(25.0,circ.getRaio(),0.0);
        }
        
}
