/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l5e3_sistemarh;

/**
 *
 * @author adams
 */
public class Gerente extends Funcionario {
    private int funcionarioIdx = 0;
    private final Funcionario[] funcionarios; 
    
    public Gerente(String nome, int idade, double salario, String dataAdmissao, int numeroFuncionarios) {
        super(nome,idade,salario,dataAdmissao);
        this.funcionarios = new Funcionario[numeroFuncionarios];
    }
    
    public Funcionario[] getFuncionarios() {
        return funcionarios;
    }
    
    public void AddFuncionario(Funcionario func) {
        if(!func.equals(this)) {
            if(funcionarioIdx < funcionarios.length)
                funcionarios[funcionarioIdx++] = func; 
        }
    }

    @Override
    public double getBonus() {
        return 2*getSalario() + 100*(funcionarioIdx+1);
    }
}
