/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s08jdbc.autorlivro;

import java.util.Comparator;
import org.ufpr.lpoo.jdbc.autorlivro.Autor;

/**
 *
 * @author adams
 */
public class ComparadorAutor implements Comparator<Autor> {

    @Override
    public int compare(Autor o1, Autor o2) {
        return o1.getNome().compareToIgnoreCase(o2.getNome());
    }
    
}
