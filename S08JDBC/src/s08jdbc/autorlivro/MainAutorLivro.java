/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s08jdbc.autorlivro;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import org.ufpr.lpoo.jdbc.autorlivro.Autor;
import org.ufpr.lpoo.jdbc.autorlivro.AutorDAO;
import org.ufpr.lpoo.jdbc.autorlivro.Livro;
import org.ufpr.lpoo.jdbc.autorlivro.LivroDAO;

/**
 *
 * @author adams
 */
public class MainAutorLivro {

    public static void main(String args[]) throws Exception {
        String opcao = "";
        while (true) {
            try {
                System.out.println("Escolha uma das opções e tecle <ENTER>: ");
                System.out.println("  1 - Incluir Autor");
                System.out.println("  2 - Incluir Livro");
                System.out.println("  3 - Listar Autores");
                System.out.println("  4 - Listar Livro com autores");
                System.out.println("  5 - Listar Autores de um livro");
                System.out.println("  6 - Listar Livros de um autor");
                System.out.println("  7 - Sair");
                Scanner sc = new Scanner(System.in, "ISO-8859-1");
                opcao = sc.nextLine();
                switch (opcao) {
                    case "1":
                        incluirAutor();
                        break;
                    case "2":
                        incluirLivro();
                        break;
                    case "3":
                        listarAutores();
                        break;
                    case "4":
                        listarLivroComAutores();
                        break;
                    case "5":
                        listarAutoresLivro();
                        break;
                    case "6":
                        listarLivrosAutor();
                        break;
                    case "7":
                        break;
                    default:
                        System.out.println("Opção inválida.");
                        break;
                }
                if (opcao.equals("7")) {
                    break;
                }
            } catch (Exception ex) {
                System.out.println("Falha na operação. Mensagem=" + ex.getMessage());
                //ex.printStackTrace();
            }
        }
    }

    public static void incluirAutor() throws Exception {
        System.out.print("Digite o nome do autor:");
        Scanner sc = new Scanner(System.in, "ISO-8859-1");
        String nome = sc.nextLine();
        Autor autor = new Autor(nome);
        List<Livro> livros = new ArrayList<>();
        int numLivros = 1;
        int idLivro = 0;
        do {
            Scanner sc2 = new Scanner(System.in, "ISO-8859-1");
            System.out.print("ID Livro " + numLivros + " (-1 para sair):");
            idLivro = sc2.nextInt();
            if (idLivro == -1) {
                break;
            }
            Livro livro = LivroDAO.getBookById(idLivro);
            if (livro != null) {
                autor.addLivro(livro);
                ++numLivros;
            } else {
                System.err.println("Livro não existe!");
            }
        } while (true);
        AutorDAO.persist(autor);
    }

    public static void incluirLivro() throws Exception {
        System.out.print("Digite o título do livro:");
        Scanner sc = new Scanner(System.in, "ISO-8859-1");
        String titulo = sc.nextLine();
        int numAutores = 1;
        List<Autor> listaAutores = new ArrayList();
        int idAutor = 0;
        do {
            try {
                Scanner sc2 = new Scanner(System.in, "ISO-8859-1");
                System.out.print("ID Autor " + numAutores + " (-1 para sair):");
                idAutor = sc2.nextInt();
                if (idAutor == -1) {
                    break;
                }
                Autor autor = AutorDAO.getAuthorById(idAutor);
                if (autor != null) {
                    listaAutores.add(autor);
                    numAutores++;
                } else {
                    System.out.println("Autor não existe!");
                }
            } catch (Exception ex) {
                System.out.println("ID autor não é inteiro ou inválido!");
            }
        } while (true);

        Livro livro = new Livro(titulo, listaAutores);
        LivroDAO.persist(livro);
    }

    public static void listarAutores() throws Exception {
        List<Autor> listaAutores = AutorDAO.listarAutores();
        //Collections.sort(listaAutores, (Autor arg0, Autor arg1) -> arg0.getNome().compareToIgnoreCase(arg1.getNome()));
        Collections.sort(listaAutores, new ComparadorAutor());
        System.out.println("ID\tNOME");
        for (Autor autor : listaAutores) {
            System.out.println(autor.getId() + " \t" + autor.getNome());
        }
    }

    public static void listarLivroComAutores() throws Exception {
        List<Livro> listaLivros = LivroDAO.listBooksWithAuthors();
        Collections.sort(listaLivros, new Comparator<Livro>() {
            public int compare(Livro arg0, Livro arg1) {
                String titulo = arg0.getTitulo();
                return titulo.compareToIgnoreCase(arg1.getTitulo());
            }
        });
        System.out.println("ID\tTitulo do Livro\tAutores");
        for (Livro livro : listaLivros) {
            System.out.print(livro.getId() + "\t" + livro.getTitulo() + "\t");
            for (Autor autor : livro.getAutores()) {
                System.out.print(autor.getNome() + ";");
            }
            System.out.print("\n");
        }

    }

    public static void listarAutoresLivro() throws Exception {
        Livro livro;
        do {
            Scanner sc = new Scanner(System.in, "ISO-8859-1");
            System.out.print("ID Livro (-1 para cancelar):");
            int idLivro = sc.nextInt();
            if (idLivro == -1) {
                throw new Exception("Ação cancelada pelo usuário.");
            }
            livro = LivroDAO.getBookById(idLivro);
            if (livro == null) {
                System.err.println("Livro não existe, tente novamente.");
            } else {
                List<Autor> autores = livro.getAutores();
                Collections.sort(autores, new ComparadorAutor());
                System.out.println("Autores do Livro " + livro.getTitulo());
                System.out.println("ID\tNOME");
                for(Autor autor : autores) {
                    System.out.println(autor.getId()+"\t"+autor.getNome());
                }
                break;
            }

        } while (true);
    }
    
    public static void listarLivrosAutor() throws Exception {
        Autor autor;
        do {
            Scanner sc = new Scanner(System.in,"ISO-8859-1");
            System.out.print("ID Autor (-1 para cancelar): ");
            int idAutor = sc.nextInt();
            if(idAutor == -1) {
                throw new Exception("Ação cancelada pelo usuário.");
            }
            autor = AutorDAO.getAuthorById(idAutor);
            if(autor == null) {
                System.err.println("Autor não existe!");
            } else {
                List<Livro> livros = autor.getLivros();
                Collections.sort(livros, (Livro o1, Livro o2) -> o1.getTitulo().compareToIgnoreCase(o2.getTitulo()));
                System.out.println("Livros do Autor "+autor.getNome());
                System.out.println("ID\tTITULO");
                for(Livro livro : livros) {
                    System.out.println(livro.getId()+"\t"+livro.getTitulo());
                }
                break;
            }
        } while(true);
    }
}
