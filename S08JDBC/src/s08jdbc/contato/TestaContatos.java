/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s08jdbc.contato;

import org.ufpr.lpoo.jdbc.contato.ContatoDAO;
import org.ufpr.lpoo.jdbc.contato.Contato;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author adams
 */
public class TestaContatos {
    public static void main(String args[]) {
        ContatoDAO dao = new ContatoDAO();
        imprime(dao.lista());
    }
    
    private static void imprime(List<Contato> lista) {
        SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
        for(Contato contato : lista) {
            System.out.println("***********************************************");
            System.out.println("Nome: "+contato.getNome());
            System.out.println("Endereço: "+contato.getEndereco());
            System.out.println("Email: "+contato.getEmail());
            System.out.println("Data Nascimento: "+fmt.format(contato.getDataNascimento().getTime()));
            
        }
    }
}
