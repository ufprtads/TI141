/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l4pessoa;

/**
 *
 * @author adams
 */
public class Pessoa {
    private String nome; 
    private String endereco; 
    private int idade; 
    
    public Pessoa() {
       this("", "", 0);
    }
    
    public Pessoa(String nome, String endereco, int idade) {
        this.nome = nome; 
        this.endereco = endereco; 
        this.idade = idade;
    }
    
    public String getNome() {
        return this.nome; 
    }
    
    public void setNome(String value) {
        this.nome = value;
    }
    
    public int getIdade() {
        return this.idade;
    }
    
    public void setIdade(int value) {
        this.idade = value;
    }
    
    public String getEndereco() {
        return this.endereco;
    }
    
    public void setEndereco(String value) {
        this.endereco = value;
    }
    
    public void fazAniversario() {
        this.idade++;
    }
    
    public void imprime() {
        System.out.println("************************************************");
        System.out.printf("Nome: %s\n", this.nome);
        System.out.printf("Idade: %d\n", this.idade);
        System.out.printf("Endereco: %s\n", this.endereco);
        System.out.println("************************************************");
    }
}
