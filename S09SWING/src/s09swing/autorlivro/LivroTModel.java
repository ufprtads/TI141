/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s09swing.autorlivro;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.ufpr.lpoo.jdbc.autorlivro.Autor;
import org.ufpr.lpoo.jdbc.autorlivro.Livro;

/**
 *
 * @author adams
 */
public class LivroTModel extends AbstractTableModel {

    private String[] columns = { "ID", "Título", "Autores" };
    
    private List<Livro> data;
    
    public LivroTModel() {
        this(false);
    }
    
    public LivroTModel(boolean hideAuthors) {
        this.data = new ArrayList<>();
        if(hideAuthors) {
            this.columns = new String[] { "ID", "Título" };
        }
    }
    
    @Override
    public int getRowCount() {
        return this.data.size();
    }

    @Override
    public int getColumnCount() {
        return this.columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Livro livro = this.data.get(rowIndex);
        String ret = "";
        switch(columnIndex) {
            case 0:
                ret = livro.getId()+"";
                break;
            case 1:
                ret = livro.getTitulo();
                break;
            case 2:
                String[] autores = new String[livro.getAutores().size()];
                int pos = 0;
                for(Autor autor : livro.getAutores()) {
                    autores[pos++] = autor.getNome();
                }
                ret = String.join(";", autores);
                break;
            default:
                break;
        }
        return ret;
    }
    @Override
    public String getColumnName(int col) {
        return columns[col];
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
    public void setData(List<Livro> livros) {
        this.data = livros; 
        update();
    }
    
    public List<Livro> getData() {
        return this.data;
    }
    
    public void addItem(Livro livro) {
        this.data.add(livro);
        this.fireTableRowsInserted(data.size() - 1, data.size() - 1);
    }
    
    public void removeItem(Livro livro) {
        int row = data.indexOf(livro);
        data.remove(livro);
        this.fireTableRowsDeleted(row, row);
    }
    
    public void update() {
        this.fireTableDataChanged();
    }
    
    public Livro getAtIndex(int row) {
        return this.data.get(row);
    }
}
