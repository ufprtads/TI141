/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s09swing.autorlivro;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.ufpr.lpoo.jdbc.autorlivro.Autor;

/**
 *
 * @author adams
 */
public class AutorTModel extends AbstractTableModel {

    private final String[] columns = {"ID", "Nome"};
    private List<Autor> data;

    public AutorTModel() {
        this.data = new ArrayList<>();
    }
    
    @Override
    public int getRowCount() {
        return this.data.size();
    }

    @Override
    public int getColumnCount() {
        return this.columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Autor autor = data.get(rowIndex);
        String ret = "";
        switch(columnIndex) {
            case 0:
                ret = autor.getId()+"";
                break;
            case 1:
                ret = autor.getNome();
                break;
            default:
                break;
        }
        return ret;
    }

    @Override
    public String getColumnName(int col) {
        return columns[col];
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
    public void setData(List<Autor> autores) {
        this.data = autores; 
        update();
    }
    
    public List<Autor> getData() {
        return this.data;
    }
    
    public void addItem(Autor autor) {
        this.data.add(autor);
        this.fireTableRowsInserted(data.size() - 1, data.size() - 1);
    }
    
    public void removeItem(Autor autor) {
        int row = data.indexOf(autor);
        data.remove(autor);
        this.fireTableRowsDeleted(row, row);
    }
    
    public void update() {
        this.fireTableDataChanged();
    }
    
    public Autor getAtIndex(int row) {
        return this.data.get(row);
    }
}
