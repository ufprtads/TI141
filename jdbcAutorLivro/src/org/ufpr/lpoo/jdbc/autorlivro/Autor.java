/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.lpoo.jdbc.autorlivro;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author adams
 */
public class Autor {
    private int id; 
    private String nome;
    private List<Livro> livros;

    public Autor() {
        this(null);
    }
    
    public Autor(String nome) {
        this.nome = nome;
        this.livros = new ArrayList<>();
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Livro> getLivros() {
        return livros;
    }

    public void setLivros(List<Livro> livros) {
        this.livros = livros;
    }

    public void addLivro(Livro livro) {
        if(!this.livros.contains(livro)) {
            this.livros.add(livro);
            livro.addAutor(this);
        }
    }
    
    public void delLivro(Livro livro) {
        if(this.livros.contains(livro)) {
            this.livros.remove(livro);
            livro.delAutor(this);
        }
    }

}
