/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.lpoo.jdbc.autorlivro;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.ufpr.lpoo.jdbc.ConnectionFactory;

/**
 *
 * @author adams
 */
public class LivroDAO {

    private static final String insertStmt = "INSERT INTO livro (titulo) VALUES (?)";
    private static final String updateStmt = "UPDATE livro SET titulo=? WHERE id=?";
    private static final String deleteStmt = "DELETE FROM livro WHERE id=? CASCADE";
    private static final String insertRelationshipStmt = "INSERT INTO livro_autor (idLivro, idAutor) VALUES (?, ?) ON DUPLICATE KEY UPDATE idAutor=idAutor, idLivro=idLivro";
    private static final String getBooksStmt = "SELECT * FROM livro";
    private static final String getBookByIdStmt = getBooksStmt + " WHERE id=?";
    private static final String getBooksByAuthorStmt = "SELECT * FROM livro WHERE id IN (SELECT idLivro FROM livro_autor WHERE idAutor=?)";

    public static void persist(Livro livro) throws Exception {
        persist(livro, false);
    }

    public static void persist(Livro livro, boolean norels) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = ConnectionFactory.getConnection();
            if (livro.getId() > 0) {
                stmt = conn.prepareStatement(updateStmt, PreparedStatement.RETURN_GENERATED_KEYS);
                stmt.setInt(2, livro.getId());
            } else {
                stmt = conn.prepareStatement(insertStmt, PreparedStatement.RETURN_GENERATED_KEYS);
            }
            stmt.setString(1, livro.getTitulo());
            stmt.executeUpdate();
            readId(stmt, livro);
            if (!norels) {
                persistRelationship(livro, conn);
            }
        } catch (SQLException ex) {
            throw new Exception(ex);
        } finally {
            try {
                stmt.close();
            } catch (Exception ex) {
            }
            try {
                conn.close();
            } catch (Exception ex) {
            }
        }
    }

    public static void detach(Livro livro) throws Exception {
        if (livro.getId() > 0) {
            Connection conn = null;
            PreparedStatement stmt = null;
            try {
                conn = ConnectionFactory.getConnection();
                stmt = conn.prepareStatement(deleteStmt);
                stmt.setInt(1, livro.getId());
                stmt.executeUpdate();
                livro.setId(-1);
            } catch (SQLException ex) {
                throw new Exception(ex);
            } finally {
                try {
                    stmt.close();
                } catch (Exception ex) {
                }
                try {
                    conn.close();
                } catch (Exception ex) {
                }
            }
        }
    }

    private static void persistRelationship(Livro livro, Connection conn) throws Exception {
        PreparedStatement stmt = null;
        try {
            if (livro.getAutores().size() > 0) {
                int pos = 0;
                stmt = conn.prepareStatement(insertRelationshipStmt);
                stmt.setInt(1, livro.getId());
                for (Autor autor : livro.getAutores()) {
                    if (autor.getId() <= 0) {
                        AutorDAO.persist(autor, true);
                    }
                    stmt.setInt(2, autor.getId());
                    stmt.executeUpdate();
                }
                stmt.close();
            }
        } catch (SQLException ex) {
            throw new Exception(ex);
        } finally {
            try {
                stmt.close();
            } catch (Exception ex) {
            }
        }
    }

    private static void readId(PreparedStatement stmt, Livro livro) throws SQLException {
        if (livro.getId() <= 0) {
            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            livro.setId(rs.getInt(1));
            rs.close();
        }
    }

    public static Livro getBookById(int id) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            stmt = conn.prepareStatement(getBookByIdStmt);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return null;
            }
            Livro livro = instantiateFromResultSet(rs);
            livro.setAutores(AutorDAO.getAuthorsByBook(livro));
            return livro;
        } catch (SQLException ex) {
            throw new Exception(ex);
        } finally {
            try {
                rs.close();
            } catch (Exception ex) {
            }
            try {
                stmt.close();
            } catch (Exception ex) {
            }
            try {
                conn.close();
            } catch (Exception ex) {
            }
        }
    }

    public static List<Livro> listBooksByAuthor(Autor autor) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            stmt = conn.prepareStatement(getBooksByAuthorStmt);
            stmt.setInt(1, autor.getId());
            rs = stmt.executeQuery();
            List<Livro> livros = new ArrayList<>();
            while(rs.next()) {
                livros.add(instantiateFromResultSet(rs));
            }
            return livros;
        } catch (SQLException ex) {
            throw new Exception(ex);
        } finally {
            try {
                rs.close();
            } catch (Exception ex) {
            }
            try {
                stmt.close();
            } catch (Exception ex) {
            }
            try {
                conn.close();
            } catch (Exception ex) {
            }
        }
    }

    public static List<Livro> listBooksWithAuthors() throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            stmt = conn.prepareStatement(getBooksStmt);
            rs = stmt.executeQuery();
            List<Livro> livros = new ArrayList<>();
            while (rs.next()) {
                Livro livro = instantiateFromResultSet(rs);
                livro.setAutores(AutorDAO.getAuthorsByBook(livro));
                livros.add(livro);
            }
            return livros;
        } catch (SQLException ex) {
            throw new Exception(ex);
        } finally {
            try {
                rs.close();
            } catch (Exception ex) {
            }
            try {
                stmt.close();
            } catch (Exception ex) {
            }
            try {
                conn.close();
            } catch (Exception ex) {
            }
        }
    }

    private static Livro instantiateFromResultSet(ResultSet rs) throws SQLException {
        Livro livro = new Livro();
        livro.setId(rs.getInt(1));
        livro.setTitulo(rs.getString(2));
        return livro;
    }

}
