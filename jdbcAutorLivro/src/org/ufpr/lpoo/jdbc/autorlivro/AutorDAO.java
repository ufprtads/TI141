/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.lpoo.jdbc.autorlivro;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.ufpr.lpoo.jdbc.ConnectionFactory;

/**
 *
 * @author adams
 */
public class AutorDAO {

    private static final String insertStmt = "INSERT INTO autor (nome) VALUES (?)";
    private static final String updateStmt = "UPDATE autor SET nome=? WHERE id=?";
    private static final String deleteStmt = "DELETE FROM autor WHERE id=? CASCADE";
    private static final String insertRelationshipStmt = "INSERT INTO livro_autor (idAutor, idLivro) VALUES (?, ?) ON DUPLICATE KEY UPDATE idAutor=idAutor, idLivro=idLivro";
    private static final String getAuthorsStmt = "SELECT * FROM autor";
    private static final String getByIdStmt = getAuthorsStmt + " WHERE id=?";
    private static final String listAuthorsByBook = "SELECT * FROM autor WHERE id IN (SELECT idAutor FROM livro_autor WHERE idLivro=?)";

    public static void persist(Autor autor) throws Exception {
        persist(autor, false);
    }

    public static void persist(Autor autor, boolean norels) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = ConnectionFactory.getConnection();
            if (autor.getId() > 0) {
                stmt = conn.prepareStatement(updateStmt, PreparedStatement.RETURN_GENERATED_KEYS);
                stmt.setInt(2, autor.getId());
            } else {
                stmt = conn.prepareStatement(insertStmt, PreparedStatement.RETURN_GENERATED_KEYS);
            }
            stmt.setString(1, autor.getNome());
            stmt.executeUpdate();
            readId(stmt, autor);
            if (!norels) {
                persistRelationship(autor, conn);
            }
        } catch (SQLException ex) {
            throw new Exception(ex);
        } finally {
            try {
                stmt.close();
            } catch (Exception ex) {
            }
            try {
                conn.close();
            } catch (Exception ex) {
            }
        }
    }

    private static void persistRelationship(Autor autor, Connection conn) throws Exception {
        PreparedStatement stmt = null;
        try {
            if (autor.getLivros().size() > 0) {
                String[] livros = new String[autor.getLivros().size()];
                int pos = 0;
                stmt = conn.prepareStatement(insertRelationshipStmt);
                stmt.setInt(1, autor.getId());
                for (Livro livro : autor.getLivros()) {
                    if (livro.getId() <= 0) {
                        LivroDAO.persist(livro);
                    }
                    stmt.setInt(2, livro.getId());
                    stmt.executeUpdate();
                }
                stmt.close();
            }
        } catch (SQLException ex) {
            throw new Exception(ex);
        } finally {
            try {
                stmt.close();
            } catch (Exception ex) {
            }
        }

    }

    private static void readId(PreparedStatement stmt, Autor autor) throws SQLException {
        if (autor.getId() <= 0) {
            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            autor.setId(rs.getInt(1));
            rs.close();
        }
    }

    public static void detach(Autor autor) throws Exception {
        if (autor.getId() > 0) {
            Connection conn = null;
            PreparedStatement stmt = null;
            try {
                conn = ConnectionFactory.getConnection();
                stmt = conn.prepareStatement(deleteStmt);
                stmt.setInt(1, autor.getId());
                stmt.executeUpdate();
                autor.setId(-1);
            } catch (SQLException ex) {
                throw new Exception(ex);
            } finally {
                try {
                    stmt.close();
                } catch (Exception ex) {
                }
                try {
                    conn.close();
                } catch (Exception ex) {
                }
            }
        }

    }

    public static List<Autor> getAuthorsByBook(Livro livro) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            stmt = conn.prepareStatement(listAuthorsByBook);
            stmt.setInt(1, livro.getId());
            rs = stmt.executeQuery();
            List<Autor> autores = new ArrayList<>();
            while (rs.next()) {
                autores.add(instantiateFromResultSet(rs));
            }
            return autores;
        } catch (SQLException ex) {
            throw new Exception(ex);
        } finally {
            try {
                rs.close();
            } catch (Exception ex) {
            }
            try {
                stmt.close();
            } catch (Exception ex) {
            }
            try {
                conn.close();
            } catch (Exception ex) {
            }
        }

    }

    public static Autor getAuthorById(int id) throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            stmt = conn.prepareStatement(getByIdStmt);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                return null;
            }
            Autor autor = instantiateFromResultSet(rs);
            autor.setLivros(LivroDAO.listBooksByAuthor(autor));
            return autor;
        } catch (SQLException ex) {
            throw new Exception(ex);
        }
    }

    public static List<Autor> listarAutores() throws Exception {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = ConnectionFactory.getConnection();
            stmt = conn.prepareStatement(getAuthorsStmt);
            rs = stmt.executeQuery();
            List<Autor> autores = new ArrayList<>();
            while (rs.next()) {
                autores.add(instantiateFromResultSet(rs));
            }
            return autores;
        } catch (SQLException ex) {
            throw new Exception(ex);
        } finally {
            try {
                rs.close();
            } catch (Exception ex) {
            }
            try {
                stmt.close();
            } catch (Exception ex) {
            }
            try {
                conn.close();
            } catch (Exception ex) {
            }
        }
    }

    private static Autor instantiateFromResultSet(ResultSet rs) throws SQLException {
        Autor autor = new Autor();
        autor.setId(rs.getInt(1));
        autor.setNome(rs.getString(2));
        return autor;
    }
}
