/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.lpoo.jdbc.autorlivro;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author adams
 */
public class Livro {
    private int id; 
    private String titulo; 
    private List<Autor> autores; 
    
    
    public Livro() {
        this(null, new ArrayList<>());
    }
    public Livro(String titulo) {
        this(titulo, new ArrayList<>());
    }
    public Livro(String titulo, List<Autor> autores) {
        this.titulo = titulo; 
        this.autores = autores;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<Autor> getAutores() {
        return autores;
    }

    public void setAutores(List<Autor> autores) {
        this.autores = autores;
    }
    
    public void addAutor(Autor autor) {
        if(!this.autores.contains(autor)) {
            this.autores.add(autor);
            autor.addLivro(this);
        }
    }
    
    public void delAutor(Autor autor) {
        if(this.autores.contains(autor)) {
            this.autores.remove(autor);
            autor.delLivro(this);
        }
    }
}
