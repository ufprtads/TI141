package org.ufpr.lpoo.jdbc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionFactory {

    public static Connection getConnection() throws Exception {
        try {
            Properties p = loadConfiguration();
            return getConnection(p);
        } catch (IOException | ClassNotFoundException | SQLException ex) {
            System.err.println(ex.getMessage());
            throw new Exception(ex);
        }
    }

    private static Connection getConnection(Properties props) throws ClassNotFoundException, SQLException {
        Class.forName(props.getProperty("db.driver"));
        String jdbcUrl = "jdbc:mysql://"+props.getProperty("db.host")+"/"+props.getProperty("db.name");
        return DriverManager.getConnection(jdbcUrl, props.getProperty("db.username"), props.getProperty("db.password"));
    }
    
    private static Properties loadConfiguration() throws IOException {
        Properties props = new Properties(); 
        props.load(ConnectionFactory.class.getResourceAsStream("db.properties"));
        return props;
    }

}
