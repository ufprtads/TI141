/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ufpr.lpoo.jdbc.contato;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.ufpr.lpoo.jdbc.ConnectionFactory;


/**
 *
 * @author adams
 */
public class ContatoDAO {

    private String insertStmt = "INSERT INTO contatos (nome, email, endereco, dataNascimento) VALUES (?, ?, ?, ?)";
    private String updateStmt = "UPDATE contatos SET nome=?, email=? endereco =?, dataNascimento=? WHERE id=?";
    private String removeStmt = "DELETE FROM contatos WHERE id=?";
    private String listStmt = "SELECT nome, email, endereco, dataNascimento FROM contatos";

    public void insere(Contato contato) {
        Connection cnx = null;
        PreparedStatement stmt = null;
        try {
            cnx = ConnectionFactory.getConnection();
            stmt = cnx.prepareStatement(insertStmt, PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, contato.getNome());
            stmt.setString(2, contato.getEmail());
            stmt.setString(3, contato.getEndereco());
            stmt.setDate(4, new Date(contato.getDataNascimento().getTimeInMillis()));
            stmt.executeUpdate();
            contato.setId(getLastID(stmt));
        } catch (Exception ex) {
            throw new RuntimeException("Erro ao inserir contato (" + ex.getMessage() + ")");
        } finally {
            try {
                stmt.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao encerrar stmt (" + ex.getMessage() + ")");
            }
            try {
                cnx.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao encerrar cnx (" + ex.getMessage() + ")");
            }
        }
    }

    public void altera(Contato contato) {
        Connection cnx = null;
        PreparedStatement stmt = null;
        try {
            cnx = ConnectionFactory.getConnection();
            stmt = cnx.prepareStatement(updateStmt);
            stmt.setString(1, contato.getNome());
            stmt.setString(2, contato.getEmail());
            stmt.setString(3, contato.getEndereco());
            stmt.setDate(4, new Date(contato.getDataNascimento().getTimeInMillis()));
            stmt.executeUpdate();
        } catch (Exception ex) {
            throw new RuntimeException("Erro ao atualizar contato (" + ex.getMessage() + ")");
        } finally {
            try {
                stmt.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao encerrar stmt (" + ex.getMessage() + ")");
            }
            try {
                cnx.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao encerrar cnx (" + ex.getMessage() + ")");
            }
        }
    }

    public void remove(Contato contato) {
        Connection cnx = null;
        PreparedStatement stmt = null;
        try {
            cnx = ConnectionFactory.getConnection();
            stmt = cnx.prepareStatement(removeStmt);
            stmt.setLong(1, contato.getId());
            stmt.executeUpdate();
        } catch (Exception ex) {
            throw new RuntimeException("Erro ao excluir contato (" + ex.getMessage() + ")");
        } finally {
            try {
                stmt.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao encerrar stmt (" + ex.getMessage() + ")");
            }
            try {
                cnx.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao encerrar cnx (" + ex.getMessage() + ")");
            }
        }
    }

    private Long getLastID(PreparedStatement stmt) throws SQLException {
        ResultSet rs = stmt.getGeneratedKeys();
        rs.next();
        Long id = rs.getLong(1);
        rs.close();
        return id;
    }

    public List<Contato> lista() {
        Connection cnx = null;
        PreparedStatement stmt = null;
        List<Contato> contatos;
        ResultSet rs = null;
        try {
            cnx = ConnectionFactory.getConnection();
            stmt = cnx.prepareStatement(listStmt);
            rs = stmt.executeQuery();
            contatos = new ArrayList<Contato>();
            while (rs.next()) {
                Date dt = rs.getDate(4);
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(dt.getTime());
                Contato contato = new Contato(rs.getString(1), rs.getString(2), rs.getString(3), cal);
                contatos.add(contato);
            }
            return contatos;
        } catch (Exception ex) {
            throw new RuntimeException("Erro ao recuperar lista de contatos (" + ex.getMessage() + ")");
        } finally {
            try {
                rs.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao encerrar resultset (" + ex.getMessage() + ")");
            }
            try {
                stmt.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao encerrar stmt (" + ex.getMessage() + ")");
            }
            try {
                cnx.close();
            } catch (SQLException ex) {
                System.err.println("Erro ao encerrar cnx (" + ex.getMessage() + ")");
            }
        }

    }
}
