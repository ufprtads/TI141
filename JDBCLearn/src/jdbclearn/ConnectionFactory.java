/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbclearn;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author adams
 */
public class ConnectionFactory {
    
    public static Connection getConnection() {
        try {
            Properties p = loadProperties();
            Class.forName(p.getProperty("db.driver"));
            return DriverManager.getConnection(p.getProperty("db.url"), p.getProperty("db.username"), p.getProperty("db.password"));
        } catch(IOException | ClassNotFoundException | SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    private static Properties loadProperties() throws IOException {
        Properties props = new Properties(); 
        props.load(ConnectionFactory.class.getResourceAsStream("app.properties"));
        return props;
    }
}
