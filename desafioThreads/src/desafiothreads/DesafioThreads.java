package desafiothreads;

import java.util.Calendar;
import java.util.Random;

public class DesafioThreads {

    private int[][] mat1, mat2, resultado;

    public DesafioThreads(int dimensao) {
        this.criaMatrizes(dimensao);
    }

    public void imprimeMatrizes() {
        System.out.println("MAT1: ");
        for (int i = 0; i < mat1.length; ++i) {
            System.out.print("L" + (i + 1) + ":\t");
            for (int j = 0; j < mat1.length; ++j) {
                System.out.print(mat1[i][j] + "\t");
            }
            System.out.println("");
        }
        System.out.println("MAT2: ");
        for (int i = 0; i < mat2.length; ++i) {
            System.out.print("L" + (i + 1) + ":\t");
            for (int j = 0; j < mat2.length; ++j) {
                System.out.print(mat2[i][j] + "\t");
            }
            System.out.println("");
        }
        System.out.println("RESULTADO: ");
        for (int i = 0; i < resultado.length; ++i) {
            System.out.print("L" + (i + 1) + ":\t");
            for (int j = 0; j < resultado.length; ++j) {
                System.out.print(resultado[i][j] + "\t");
            }
            System.out.println("");
        }
    }

    private void criaMatrizes(int dimensao) {
        mat1 = new int[dimensao][dimensao];
        mat2 = new int[dimensao][dimensao];
        resultado = new int[dimensao][dimensao];
        Random rdr = new Random();
        for (int i = 0; i < dimensao; ++i) {
            for (int j = 0; j < dimensao; ++j) {
                mat1[i][j] = rdr.nextInt(100);
                mat2[i][j] = rdr.nextInt(100);
            }
        }
    }

    private void produtoLinhaColuna(int linha, int coluna) {
        resultado[linha][coluna] = 0;
        for (int i = 0; i < mat1.length; ++i) {
            resultado[linha][coluna] += mat1[linha][i] * mat2[i][coluna];
        }
    }

    public void multiplicaIntervalo(int inicio, int fim) {
        for (int linha = inicio; linha < fim; ++linha) {
            for (int coluna = 0; coluna < mat2.length; ++coluna) {
                produtoLinhaColuna(linha, coluna);
            }
        }
    }

    public void multiplica(int threads) {
        ThreadGroup group = new ThreadGroup("DesafioThreads");
        group.setDaemon(true);
        int linhasPorThread = mat1.length / threads;
        int inicio = 0;
        if (linhasPorThread == 0) {
            System.err.println("Erro: o número de linhas por thread é igual a zero!");
        }
        final DesafioThreads dt = this;
        for (int ti = 0; ti < threads; ++ti) {
            int fim = inicio + linhasPorThread;
            if (fim > mat1.length) {
                fim = mat1.length;
            }
            final int tinicio = inicio;
            final int tfim = fim;
            Thread t = new Thread(group, new Runnable() {
                @Override
                public void run() {
                    Thread.currentThread().setName(String.format("MultiplicandoLinha-%d-a-Linha-%d", tinicio, tfim));
                    dt.multiplicaIntervalo(tinicio, tfim);
                }
            });
            t.start();
            inicio = fim;
        }
        while (!group.isDestroyed()) {
        }
    }

    public static void main(String[] args) {
        int[] dimensoes = {50, 100, 150, 200, 400, 1000};
        for (int dimensao : dimensoes) {
            System.out.println(String.format("Matriz %dx%d", dimensao, dimensao));
            DesafioThreads dt = new DesafioThreads(dimensao);
            int[] threadCount = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            for (int tc : threadCount) {
                System.out.print(String.format("Matriz %dx%d, Threads: %d", dimensao, dimensao, tc));
                long start = Calendar.getInstance().getTimeInMillis();
                dt.multiplica(tc);
                long end = Calendar.getInstance().getTimeInMillis();
                System.out.println(String.format("tempo (ms): %d", end - start));
            }
        }
    }

}
