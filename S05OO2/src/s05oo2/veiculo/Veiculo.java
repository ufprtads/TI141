/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s05oo2.veiculo;

/**
 *
 * @author adams
 */
public abstract class Veiculo {
    private String modelo; 
    public Veiculo(String modelo) {
        setModelo(modelo);
    }
    
    public String getModelo() {
        return modelo;
    }
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
    public abstract void andar(String movimento);
}
