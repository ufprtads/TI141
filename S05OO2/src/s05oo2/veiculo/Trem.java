/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s05oo2.veiculo;

/**
 *
 * @author adams
 */
public class Trem extends VeiculoTerrestre {

    public Trem() {
        super("Trem");
    }

    @Override
    public void andar(String movimento) {
        System.out.println(getModelo()+": "+movimento);
    }
    
}
