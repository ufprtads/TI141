/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package s05oo2.formas;

/**
 *
 * @author Rafael
 */
public class ArquivoGIF implements Desenhavel{
    private byte[] array;

    @Override
    public void desenhaFormaNaTela() {
        System.out.println("Desenho do arquivo GIF.");
    }

    @Override
    public void salvaFormaNoArquivoGIF() {
        System.out.println("Gravou o arquivo GIF.");
    }
    
    
    
}
