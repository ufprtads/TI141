/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package s05oo2.formas;

/**
 *
 * @author Rafael
 */
public interface Desenhavel {
    
    public void desenhaFormaNaTela();
    public void salvaFormaNoArquivoGIF();
    
}
