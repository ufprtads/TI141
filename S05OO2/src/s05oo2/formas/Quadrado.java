/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s05oo2.formas;

/**
 *
 * @author adams
 */
public class Quadrado extends Forma implements Superficie {
    public Quadrado(double lado) {
        super(1);
        setMedida(0,lado);
    }
    @Override
    public double area() {
        return Math.pow(getMedida(0), 2);
    }
    
}
