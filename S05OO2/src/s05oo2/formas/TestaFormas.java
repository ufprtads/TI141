/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s05oo2.formas;

/**
 *
 * @author adams
 */
public class TestaFormas {
    public static void main(String args[]) {
        Superficie sp = new Circunferencia(10);
        System.out.println(sp.area());
        sp = new Quadrado(20);
        System.out.println(sp.area());
    }
}
