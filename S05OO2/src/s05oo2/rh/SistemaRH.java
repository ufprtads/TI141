/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s05oo2.rh;

/**
 *
 * @author adams
 */
public class SistemaRH {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Funcionario[] listaFuncionarios = new Funcionario[5];
        listaFuncionarios[0] = new Diretor("Diretor Geral", 55, 12000, "10/10/2005", 2);
        Diretor dir = (Diretor) listaFuncionarios[0];
        dir.setDepartamentos(new String[] {"Financeiro", "Operacional"});
        listaFuncionarios[1] = new Gerente("Gerente Financeiro", 36, 6000, "10/05/2012", 1);
        Gerente financeiro = (Gerente) listaFuncionarios[1];
        listaFuncionarios[2] = new Gerente("Gerente Operacional", 41, 5200, "10/06/2013", 1);
        Gerente operacional = (Gerente) listaFuncionarios[2];
        listaFuncionarios[3] = new Analista("Analista 1", 20, 2600, "10/07/2015");
        listaFuncionarios[4] = new Analista("Analista 2", 20, 2600, "10/07/2015");
        financeiro.AddFuncionario(listaFuncionarios[3]);
        operacional.AddFuncionario(listaFuncionarios[4]);
        
        imprimeRelatorio(listaFuncionarios);
    }
    
    
    public static void imprimeRelatorio(Funcionario[] listaFuncionarios) {
        System.out.println("Relatório de Funcionários (cargo/nome/dt. admissao)");
        for(Funcionario func : listaFuncionarios) {
            System.out.println(func.getClass().getName()+": "+func.getNome()+"("+func.getDataAdmissao()+")");
        }
    }
}
