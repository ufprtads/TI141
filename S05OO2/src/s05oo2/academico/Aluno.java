package s05oo2.academico;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author adams
 */
public class Aluno extends Pessoa {
    private String nome, matricula, curso;
    private String[] disciplinas; 
    private int idade, periodo;  
    
    public Aluno() {
        this("", "", "", 0, 0, 0);
    }
    
    
    public Aluno(String nome, String matricula, String curso, int periodo, int idade, int quantidadeDisciplinasPermitidas) {
        super(nome, idade);
        this.matricula = matricula; 
        this.curso = curso; 
        this.periodo = periodo; 
        this.disciplinas = new String[quantidadeDisciplinasPermitidas];
    }

    /**
     * @return the matricula
     */
    public String getMatricula() {
        return matricula;
    }

    /**
     * @param matricula the matricula to set
     */
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    /**
     * @return the curso
     */
    public String getCurso() {
        return curso;
    }

    /**
     * @param curso the curso to set
     */
    public void setCurso(String curso) {
        this.curso = curso;
    }


    /**
     * @return the periodo
     */
    public int getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }
    
    /**
     * @return the disciplinas
     */
    public String[] getDisciplinas() {
        return this.disciplinas;
    }
    /**
     * @param disciplinas the disciplinas to set
     */
    public void setDisciplinas(String[] disciplinas) {
        this.disciplinas = disciplinas;
    }

    private boolean matriculado(String disciplina) {
        for(String disc : disciplinas) {
            if(disc != null && disc.equals(disciplina))
                return true;
        }
        return false;
    }
    
    public String fazMatricula(String disciplina) {
        String msg = ""; 
        if(disciplinas.length == 0) {
            msg = "Este aluno não pode ser matriculado em nenhuma disciplina, por favor, fale com a secretaria.";
        } else {
            if(!matriculado(disciplina)) {
                for(int i = 0; i < disciplinas.length; ++i) {
                    if(disciplinas[i] == null) {
                        disciplinas[i] = disciplina;
                        msg = String.format("Matrícula na disciplina \"%s\" executada.", disciplina);
                        break;
                    } else if(i == disciplinas.length - 1) {
                        msg = String.format("Quantidade de disciplinas excedida. O limite de disciplinas para este aluno é de %d disciplina(s). Se desejar, cancele a matrícula de uma das disciplinas e faça a nova matrícula.", disciplinas.length);
                    }
                }
            } else {
                msg = String.format("Já matriculado na disciplina \"%s\".", disciplina);
            }
        }
        return msg;
    }
    
    public String cancelaMatricula(String disciplina) {
        String msg = ""; 
        if(matriculado(disciplina)) {
            for(int i = 0; i < disciplinas.length; ++i) {
                if(disciplinas[i].equals(disciplina)) {
                    disciplinas[i] = null;
                    msg = String.format("Cancelamento da matrícula da disciplina %s executado com sucesso.", disciplina);
                    break;
                }
            }
        } else {
             msg = String.format("Aluno não está matriculado na disciplina %s, portando não é possível cancelar esta matrícula.", disciplina);
        }
        return msg;
    }

    public String imprime() {
        return "-----------------------------------------------------------------\n"
                + String.format("Nome do Aluno: %s\n", this.nome)
                + String.format("Matrícula: %s\n", this.matricula)
                + String.format("Curso: %s\n", this.curso) 
                + String.format("Período: %d\n", this.periodo)
                + String.format("Disciplinas Matriculadas: %s", montaStringDisciplinas())
                + "\n-----------------------------------------------------------------";
    }

    private String montaStringDisciplinas() {
        String retorno = "";
        for(int i = 0; i < disciplinas.length; ++i) {
            if(i < disciplinas.length - 1 && i > 0) {
                    retorno += "; ";
            }
            if(disciplinas[i]!=null) {
                retorno += disciplinas[i];
            }
            
        }
        retorno = retorno.replace("; $", "");
        return retorno; 
    }
    
    public int contaDisciplinas() {
        int count = 0; 
        for(String disciplina : disciplinas) {
            if(disciplina!=null)
                ++count;
        }
        return count;
    }
}
