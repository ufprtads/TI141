package s05oo2.academico;

import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author adams
 */
public class SistemaAcademico {

    private static Aluno[] alunos;

    public static void DimensionarArrayAlunos(int quantidadeAlunos) {
        alunos = new Aluno[quantidadeAlunos];
    }

    public static void cadastrarAluno(Aluno aluno) {
        for (int i = 0; i < alunos.length; ++i) {
            if (alunos[i] == null) {
                alunos[i] = aluno;
                break;
            }
            if (i == alunos.length - 1) {
                System.out.printf("Número máximo de alunos já atingido (%d alunos)\n", alunos.length);
                return;
            }
        }

    }

    public static void excluirAlunoPorNome(String alunoNome) {
        for (int i = 0; i < alunos.length; ++i) {
            if (alunos[i] != null && alunos[i].getNome().equals(alunoNome)) {
                alunos[i] = null;
                System.out.println("Aluno excluído com sucesso!");
                break;
            }
            if (i == alunos.length - 1) {
                System.out.println("Aluno não encontrado!");
            }
        }
    }

    public static Aluno[] listarAlunos() {
        return alunos;
    }

    public static String matricularAlunoEmDisciplina(Aluno aluno, String disciplina) {
        if (aluno.contaDisciplinas() == 0) {
            PerguntaQuantidadeDisciplinasAluno(aluno);
        }
        return aluno.fazMatricula(disciplina);
    }

    public static String cancelarMatricula(Aluno aluno, String disciplina) {
        return aluno.cancelaMatricula(disciplina);
    }

    public static String imprimirListaDeAlunoseDisciplinas() {
        String retorno = "";
        for (Aluno aluno : alunos) {
            if(aluno != null)
                retorno += aluno.imprime() + "\n";
        }
        return retorno;
    }

    public static Aluno pesquisarAlunoPorNome(String nome) {
        for (Aluno aluno : alunos) {
            if (aluno != null && aluno.getNome().equals(nome)) {
                return aluno;
            }
        }
        return null;
    }

    private static void PerguntaQuantidadeDisciplinasAluno(Aluno aluno) {
        do {
            try {
                System.out.print("Informe o número de disciplinas: ");
                Scanner scn = new Scanner(System.in);
                aluno.setDisciplinas(new String[scn.nextInt()]);
                break;
            } catch (Exception ex) {
                System.err.println("Número inválido (apenas números inteiros)");
            }
        } while (true);
    }
}
