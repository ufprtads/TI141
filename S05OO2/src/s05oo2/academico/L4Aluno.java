/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s05oo2.academico;

import java.util.Scanner;

/**
 *
 * @author adams
 */
public class L4Aluno {

    private static String[] menuItems = {
        "1 - Cadastrar Aluno\n",
        "2 - Excluir Aluno Por Nome\n",
        "3 - Listar Alunos\n",
        "4 - Matricular Aluno em Disciplina\n",
        "5 - Cancelar Matrícula\n",
        "6 - Imprimir Lista Alunos e Disciplinas Matriculadas\n",
        "7 - Sair\n",
        "Sua opção: "
    };

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        PerguntarQuantidadeDeAlunoseDimensionarArray();
        do {
            switch (CapturarOpcaoMenu()) {
                case 1:
                    CapturarDadosDoAlunoECadastrar();
                    break;
                case 2:
                    PerguntarNomeDoAlunoeExcluir();
                    break;
                case 3:
                    ImprimirListaAlunos();
                    break;
                case 4:
                    LocalizarAlunoEMatricularEmDisciplina();
                    break;
                case 5:
                    LocalizarAlunoECancelarMatriculaEmDisciplina();
                    break;
                case 6:
                    ImprimirAlunosEDisciplinas();
                    break;
                case 7:
                    return;
            }
        } while (true);
    }

    private static void PerguntarQuantidadeDeAlunoseDimensionarArray() {
        SistemaAcademico.DimensionarArrayAlunos(CapturarInteiro("Informe o número de alunos a cadastrar: "));
    }

    private static void ImprimeMenu() {
        for (String menuItem : menuItems) {
            System.out.print(menuItem);
        }
    }

    private static Scanner CriarScanner() {
        return new Scanner(System.in);
    }

    private static int CapturarOpcaoMenu() {
        do {
            ImprimeMenu();
            int captura = CriarScanner().nextInt();
            if (captura < 0 || captura > 7) {
                System.err.println("Opção inválida!");
            } else {
                return captura;
            }
        } while (true);
    }

    private static int CapturarInteiro(String titulo) {
        do {
            try {
                System.out.print(titulo);
                return CriarScanner().nextInt();
            } catch (Exception ex) {
                System.err.println("Formato inválido!");
            }
        } while (true);
    }

    private static String CapturarString(String titulo) {
        System.out.print(titulo);
        return CriarScanner().nextLine().replace("\n", "");
    }

    private static void CapturarDadosDoAlunoECadastrar() {
        Aluno aluno = new Aluno();
        aluno.setNome(CapturarString("Nome do Aluno: "));
        aluno.setIdade(CapturarInteiro("Idade: "));
        aluno.setCurso(CapturarString("Curso: "));
        aluno.setPeriodo(CapturarInteiro("Período: "));
        SistemaAcademico.cadastrarAluno(aluno);
        System.out.println("Aluno cadastrado");
    }

    private static void PerguntarNomeDoAlunoeExcluir() {
        SistemaAcademico.excluirAlunoPorNome(CapturarString("Informe nome do aluno para exclusão: "));
    }

    private static void ImprimirListaAlunos() {
        System.out.println("**************************");
        System.out.println("Alunos");
        System.out.println("**************************");
        for (Aluno aluno : SistemaAcademico.listarAlunos()) {
            if (aluno != null) {
                System.out.println(aluno.getNome());
            }
        }
        System.out.println("**************************");
    }

    private static void LocalizarAlunoEMatricularEmDisciplina() {
        Aluno aluno = SistemaAcademico.pesquisarAlunoPorNome(CapturarString("Informe o nome do Aluno: "));
        if(aluno != null)
            System.out.println(SistemaAcademico.matricularAlunoEmDisciplina(aluno, CapturarString("Informe a disciplina para matricular: ")));
        else 
            System.err.println("Aluno não encontrado.");
            
    }
    
    private static void LocalizarAlunoECancelarMatriculaEmDisciplina() {
        Aluno aluno = SistemaAcademico.pesquisarAlunoPorNome(CapturarString("Informe o nome do Aluno: "));
        if(aluno != null)
            System.out.println(SistemaAcademico.cancelarMatricula(aluno, CapturarString("Informe a disciplina para cancelar matrícula: ")));
        else 
            System.err.println("Aluno não encontrado.");
    }
    
    private static void ImprimirAlunosEDisciplinas() {
        System.out.println("**************************");
        System.out.println("Alunos e disciplinas");
        System.out.println("**************************");
        System.out.print(SistemaAcademico.imprimirListaDeAlunoseDisciplinas());
    }
}
