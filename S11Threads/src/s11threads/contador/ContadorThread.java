/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s11threads.contador;

/**
 *
 * @author adams
 */
public class ContadorThread extends Thread {
    private final int limite;
    private int contador = 0;
    
    public ContadorThread(int limite) {
        this.limite = limite;
        //this.setName(String.format("ContadorThread-%d", limite));
    }
    
    @Override
    public void run() {
        do {
            System.out.println(String.format("%s: %d", this.getName(), contador));
            ++contador;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                System.err.println(String.format("%s: interrupted!", this.getName()));
            }
        } while(contador <= limite);
    }
}
