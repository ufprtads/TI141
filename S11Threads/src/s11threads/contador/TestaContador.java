/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s11threads.contador;

/**
 *
 * @author adams
 */
public class TestaContador {
    public static void main(String args[]) {
        ContadorThread t1, t2; 
        message("Instanciando ContadorThread");
        t1 = new ContadorThread(10);
        t2 = new ContadorThread(20);
        message("Iniciando threads");
        t1.start();
        t2.start();
    }
    
    public static void message(String msg) {
        System.out.println(String.format("%s: %s", Thread.currentThread().getName(), msg));
    }
}
