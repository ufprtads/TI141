/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s11threads.matrizes;

/**
 *
 * @author adams
 */
public interface IMatriz {
    public void setM1(int[][] mat);
    public void setM2(int[][] mat);
    public int[][] multiplica() throws Exception;
}
