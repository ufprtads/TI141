/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s11threads.matrizes;

/**
 *
 * @author adams
 */
public class MultiplicacaoMatrizesST implements IMatriz {

    private int[][] m1, m2;

    public MultiplicacaoMatrizesST() {
    }

    public void setM1(int[][] mat) {
        this.m1 = mat;
    }

    public void setM2(int[][] mat) {
        this.m2 = mat;
    }

    public int[][] multiplica() throws Exception {
        int rrows = m1.length;
        int rcols = m2[0].length;
        if(m1[0].length != m2.length) {
            throw new Exception("Produto matricial impossível.");
        }
        int[][] result = new int[rrows][rcols];
        for(int i = 0; i < rrows; i++) {
            for(int j = 0; j < rcols; j++) {
                result[i][j] = Matrizes.produtoLinhaColuna(m1, m2,i, j);
            }
        }
        return result;
    }
}
