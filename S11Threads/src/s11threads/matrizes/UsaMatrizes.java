package s11threads.matrizes;

import java.util.Calendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author adams
 */
public class UsaMatrizes {

    public static void main(String[] args) throws Exception {
        int dimensao = 800;
        int[] tcount = new int[] { 1, 2, 4, 8, 10 };
        int[][] m1 = Matrizes.matrizRandomica(dimensao, dimensao);
        int[][] m2 = Matrizes.matrizRandomica(dimensao, dimensao);
        IMatriz mp;
        long start, end;
        for (int i = 0; i < tcount.length; ++i) {
            mp = new MultiplicacaoMatrizesMT(tcount[i]);
            mp.setM1(m1);
            mp.setM2(m2);
            start = getTime(); 
            mp.multiplica();
            end = getTime();
            System.out.println(tcount[i]+": " + (end - start));
        }
    }

    private static long getTime() {
        return Calendar.getInstance().getTimeInMillis();
    }
}
