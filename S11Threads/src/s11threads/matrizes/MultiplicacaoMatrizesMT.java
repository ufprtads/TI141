/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s11threads.matrizes;

/**
 *
 * @author adams
 */
public class MultiplicacaoMatrizesMT implements IMatriz {
    
    class MultiplicacaoThread implements Runnable {
        private int start, max, index; 
        private int[][] m1, m2, result;
        public MultiplicacaoThread(int index, int[][] m1, int[][] m2, int[][] result, int start, int max) {
            this.index = index;
            this.start = start; 
            this.max = max; 
            this.m1 = m1; 
            this.m2 = m2; 
            this.result = result;
        }
        public void run() {
            Thread.currentThread().setName(String.format("MultiplicaThread-%d", index));
            for(int i = start; i < max; ++i) {
                for(int j = 0; j < m2[i].length; ++j) {
                    result[i][j] = Matrizes.produtoLinhaColuna(m1, m2, i, j);
                }
            }
        }
    }
    
    private int[][] m1, m2; 
    private int threadCount, step; 
    private boolean[] eStatus;
    private ThreadGroup tg;
    public MultiplicacaoMatrizesMT(int threads) {
        this.threadCount = threads;
        this.eStatus = new boolean[threads];
        this.tg = new ThreadGroup("CALCULATION");
        this.tg.setDaemon(true);
    }
    
    public void setM1(int[][] mat) {
        this.m1 = mat;
    }

    public void setM2(int[][] mat) {
        this.m2 = mat;
    }

    public int[][] multiplica() throws Exception {
        int rrows = m1.length;
        int rcols = m2[0].length;
        if(m1[0].length != m2.length) {
            throw new Exception("Produto matricial impossível.");
        }
        int[][] result = new int[rrows][rcols];
        stepCalc();
        
        for(int start = 0, count = 0; start < m1.length; start += step, count++) {
            //Thread t = new MultiplicacaoThread(count, m1, m2, result, start, start + step);
            new Thread(tg, new MultiplicacaoThread(count, m1, m2, result, start, start + step)).start();
        }
        while(!tg.isDestroyed()) {
        }
        return result;
    }
    
    public void stepCalc() throws Exception {
        if(m1.length < threadCount) {
            throw new Exception(String.format("Número de threads inferior ao número de linhas de M1 (linhas: %d, threadcount: %d)", m1.length, threadCount));
        } else if(m1.length % threadCount > 0) {
            throw new Exception(String.format("Linhas/threadCount não exato (%d/%d - resto %d)", m1.length, threadCount, m1.length % threadCount));
        }
        step = m1.length/threadCount;
    }
}


