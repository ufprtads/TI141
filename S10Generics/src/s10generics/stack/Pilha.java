/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s10generics.stack;

/**
 *
 * @author adams
 */
public class Pilha<T> {

    class PilhaElement<T> {

        private T value;
        private PilhaElement<T> next;

        public PilhaElement(T value, PilhaElement<T> next) {
            this.value = value;
            this.next = next;
        }

        public T getValue() {
            return value;
        }

        public PilhaElement<T> getNext() {
            return next;
        }

        public void setNext(PilhaElement<T> next) {
            this.next = next;
        }
    }

    private PilhaElement<T> stack;

    public Pilha() {
        stack = null;
    }

    public void empilha(T value) {
        PilhaElement<T> elemento = new PilhaElement<T>(value, stack);
        stack = elemento;
    }

    public T desempilha() {
        if (stack == null) {
            throw new PilhaVaziaException();
        }
        PilhaElement<T> elemento = stack;
        stack = elemento.getNext();
        return elemento.getValue();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        PilhaElement<T> actual = stack;
        while (actual != null) {
            sb.append(actual.getValue().toString());
            actual = actual.getNext();
            if (actual != null) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
