/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s10generics.stack;

/**
 *
 * @author adams
 */
public class PilhaVaziaException extends RuntimeException {
    public PilhaVaziaException() {
        super("Nenhum elemento encontrado (pilha vazia)");
    }
}
